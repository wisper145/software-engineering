package cn.edu.cqvie.entity;

import java.io.Serializable;
import java.util.Objects;

public class ContactMultiKeys implements Serializable {

    protected Long ownerUid;
    protected Long otherUid;

    public ContactMultiKeys(Long ownerUid, Long otherUid) {
        this.ownerUid = ownerUid;
        this.otherUid = otherUid;
    }

    public ContactMultiKeys() {
        
    }

    public Long getOtherUid() {
        return otherUid;
    }

    public void setOtherUid(Long otherUid) {
        this.otherUid = otherUid;
    }

    public Long getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(Long ownerUid) {
        this.ownerUid = ownerUid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactMultiKeys that = (ContactMultiKeys) o;
        return Objects.equals(ownerUid, that.ownerUid) && Objects.equals(otherUid, that.otherUid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ownerUid, otherUid);
    }
}