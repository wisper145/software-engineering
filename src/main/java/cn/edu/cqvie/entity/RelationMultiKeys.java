package cn.edu.cqvie.entity;

import java.io.Serializable;
import java.util.Objects;

public class RelationMultiKeys implements Serializable {

    protected Long mid;
    protected Long ownerUid;

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public Long getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(Long ownerUid) {
        this.ownerUid = ownerUid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RelationMultiKeys that = (RelationMultiKeys) o;
        return Objects.equals(mid, that.mid) && Objects.equals(ownerUid, that.ownerUid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mid, ownerUid);
    }
}