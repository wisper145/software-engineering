package cn.edu.cqvie.service;


import cn.edu.cqvie.entity.User;
import cn.edu.cqvie.vo.MessageContactVO;

import java.util.List;

public interface UserService {

    User login(String email, String password);

    List<User> getAllUsersExcept(long exceptUid);

    List<User> getAllUsersExcept(User exceptUser);

    MessageContactVO getContacts(User ownerUser);
}
